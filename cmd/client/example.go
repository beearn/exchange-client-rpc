package main

import (
	"context"
	"gitlab.com/beearn/exchange-client-rpc/logs"
	"gitlab.com/beearn/exchange-client-rpc/rpc"
	"gitlab.com/beearn/exchange-client-rpc/rpc/balancer"
	"log"
	"os"
)

var (
	//Binance
	binanceAPIKey    = ""
	binanceSecretKey = ""
)

func main() {
	logger := logs.NewLogger(logs.Debug, os.Stdout)
	var servers = []string{"127.0.0.1:3080"}
	b := balancer.NewGPRCLoadBalancer(logger, servers...)
	data, err := b.KlinesFrom(context.Background(), "BTCUSDT", "1h", 1500000000000, 100, 1, rpc.Credentials{
		APIKey:    binanceAPIKey,
		SecretKey: binanceSecretKey,
	})
	if err != nil {
		log.Fatalf("error get symbols: %v", err)
	}
	log.Printf("data: %v", data)
}
