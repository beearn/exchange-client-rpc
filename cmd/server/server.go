package main

import (
	"fmt"
	"gitlab.com/beearn/exchange-client-rpc/rpc"
	"gitlab.com/beearn/exchange-client-rpc/rpc/server"
	"log"
	"net"
	"os"

	"github.com/joho/godotenv"
	"google.golang.org/grpc"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Println("Error loading .env file")
	}
	log.Println(os.Environ())
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", 3080))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	rpc.RegisterBinanceServer(s, server.NewBinanceServer())
	log.Printf("server listening at %v", lis.Addr())
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
