package adapter

import (
	"context"
	"gitlab.com/beearn/entity"
	"gitlab.com/beearn/exchange-client-rpc/logs"
	"gitlab.com/beearn/exchange-client-rpc/rpc"
	"gitlab.com/beearn/exchange-client-rpc/rpc/balancer"
	"log"
	"os"
)

const (
	Spot = iota
	Futures
)

// https://api.binance.com/api/v3/exchangeInfo
type Binance struct {
	binance   *balancer.BinanceLoadBalancer
	apiKey    string
	secretKey string
}

type Opt func(*Binance)

func WithBalancer(servers ...string) Opt {
	return func(binance *Binance) {
		binance.binance = balancer.NewGPRCLoadBalancer(logs.NewLogger("debug", os.Stdout), servers...)
	}
}

func NewBinance(apiKey, secretKey string, opts ...Opt) *Binance {
	b := &Binance{
		apiKey:    apiKey,
		secretKey: secretKey,
	}
	for _, opt := range opts {
		opt(b)
	}
	if b.binance == nil {
		log.Fatalf("binance load balancer is not set")
	}

	return b
}

func (b *Binance) GetSymbols(ctx context.Context, market int) ([]string, error) {
	return b.binance.GetSymbols(ctx, market, rpc.Credentials{
		APIKey:    b.apiKey,
		SecretKey: b.secretKey,
	})
}

func (b *Binance) Klines(ctx context.Context, symbol, period string, market int) ([]*entity.Kline, error) {
	return b.binance.Klines(ctx, symbol, period, market, rpc.Credentials{
		APIKey:    b.apiKey,
		SecretKey: b.secretKey,
	})
}
func (b *Binance) KlinesFrom(ctx context.Context, symbol, period string, startTime, limit, market int) ([]*entity.Kline, error) {
	return b.binance.KlinesFrom(ctx, symbol, period, startTime, limit, market, rpc.Credentials{
		APIKey:    b.apiKey,
		SecretKey: b.secretKey,
	})
}

func (b *Binance) GetMarketStat(ctx context.Context, market int) ([]*entity.PriceChangeStats, error) {
	return b.binance.GetMarketStat(ctx, market, rpc.Credentials{
		APIKey:    b.apiKey,
		SecretKey: b.secretKey,
	})
}

func (b *Binance) GetOrder(ctx context.Context, symbol string, market int) ([]*entity.OrderInfo, error) {
	return b.binance.GetOrder(ctx, symbol, market, rpc.Credentials{
		APIKey:    b.apiKey,
		SecretKey: b.secretKey,
	})
}

// Ticker define ticker info
func (b *Binance) Ticker(ctx context.Context, market int) ([]*entity.Crypto, error) {
	return b.binance.Ticker(ctx, market, rpc.Credentials{
		APIKey:    b.apiKey,
		SecretKey: b.secretKey,
	})
}
