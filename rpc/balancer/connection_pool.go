package balancer

import (
	"errors"
	"gitlab.com/beearn/exchange-client-rpc/rpc/client"
	"sync"
	"sync/atomic"
	"time"
)

// ErrorNoObjectsProvided is the error that occurs when no objects are provided.
var ErrorNoObjectsProvided = errors.New("no objects provided")

type RoundRobin struct {
	binanceClients []*client.BinanceClient
	allClients     []*client.BinanceClient
	mux            sync.Mutex
	next           uint32
}

func NewConnPool(addresses ...string) *RoundRobin {
	var rb RoundRobin
	for _, address := range addresses {
		rb.binanceClients = append(rb.binanceClients, client.NewBinanceClient(address))
	}

	rb.allClients = rb.binanceClients

	return &rb
}

// Next returns the next object.
func (r *RoundRobin) Next() *client.BinanceClient {
	r.mux.Lock()
	defer r.mux.Unlock()
	n := atomic.AddUint32(&r.next, 1)

	if int(n) > len(r.binanceClients) {
		atomic.StoreUint32(&r.next, 0)
		n = 1
	}

	return r.binanceClients[(int(n)-1)%len(r.binanceClients)]
}

func (r *RoundRobin) Eliminate(timeout time.Duration) {

	var availableClients []*client.BinanceClient

	for _, bc := range r.allClients {
		if bc.CheckAvailability(timeout) {
			availableClients = append(availableClients, bc)
		}
	}

	r.mux.Lock()
	r.binanceClients = availableClients
	r.mux.Unlock()
}

func (r *RoundRobin) StartElimination(timeout time.Duration, interval time.Duration) {
	go func() {
		for {
			time.Sleep(interval)
			r.Eliminate(timeout)
		}
	}()
}
