package balancer

import (
	"context"
	"gitlab.com/beearn/entity"
	"gitlab.com/beearn/exchange-client-rpc/rpc"
	"go.uber.org/zap"
	"time"
)

type BinanceLoadBalancer struct {
	client *RoundRobin
	log    *zap.Logger
	rpc.UnimplementedBinanceServer
}

func NewGPRCLoadBalancer(log *zap.Logger, addresses ...string) *BinanceLoadBalancer {
	connPool := NewConnPool(addresses...)
	connPool.StartElimination(5*time.Second, 30*time.Second)

	return &BinanceLoadBalancer{
		client: connPool,
		log:    log,
	}
}

func (b *BinanceLoadBalancer) GetSymbols(ctx context.Context, market int, creds rpc.Credentials) ([]string, error) {
	return b.client.Next().GetSymbols(ctx, market, creds)
}

func (b *BinanceLoadBalancer) Klines(ctx context.Context, symbol, period string, market int, creds rpc.Credentials) ([]*entity.Kline, error) {
	return b.client.Next().Klines(ctx, symbol, period, market, creds)
}
func (b *BinanceLoadBalancer) KlinesFrom(ctx context.Context, symbol, period string, startTime, limit, market int, creds rpc.Credentials) ([]*entity.Kline, error) {
	return b.client.Next().KlinesFrom(ctx, symbol, period, startTime, limit, market, creds)
}

func (b *BinanceLoadBalancer) GetMarketStat(ctx context.Context, market int, creds rpc.Credentials) ([]*entity.PriceChangeStats, error) {
	return b.client.Next().GetMarketStat(ctx, market, creds)
}

func (b *BinanceLoadBalancer) GetOrder(ctx context.Context, symbol string, market int, creds rpc.Credentials) ([]*entity.OrderInfo, error) {
	return b.client.Next().GetOrder(ctx, symbol, market, creds)
}

// Ticker define ticker info
func (b *BinanceLoadBalancer) Ticker(ctx context.Context, market int, creds rpc.Credentials) ([]*entity.Crypto, error) {
	return b.client.Next().Ticker(ctx, market, creds)
}
