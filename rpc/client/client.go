package client

import (
	"context"
	"gitlab.com/beearn/entity"
	"gitlab.com/beearn/exchange-client-rpc/rpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
	"net"
	"time"
)

type BinanceClient struct {
	rpc     rpc.BinanceClient
	Address string
}

func NewBinanceClient(addr string) *BinanceClient {
	conn, err := grpc.Dial(addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	c := rpc.NewBinanceClient(conn)

	return &BinanceClient{
		rpc:     c,
		Address: addr,
	}
}

func (b *BinanceClient) GetSymbols(ctx context.Context, market int, creds rpc.Credentials) ([]string, error) {
	b.log()
	var res []string
	symbols, err := b.rpc.GetSymbols(ctx, &rpc.MarketIn{
		Credentials: &creds,
		Market:      int64(market),
	})
	if err != nil {
		return res, err
	}

	res = symbols.GetSymbols()

	return res, nil
}

func (b *BinanceClient) log() {
	log.Println(b.Address)
}

func (b *BinanceClient) Klines(ctx context.Context, symbol, period string, market int, creds rpc.Credentials) ([]*entity.Kline, error) {
	b.log()
	var res []*entity.Kline

	klinesOut, err := b.rpc.Klines(ctx, &rpc.KlinesIn{
		Credentials: &creds,
		Symbol:      symbol,
		Period:      period,
		Market:      int64(market),
	})
	if err != nil {
		return res, err
	}

	for _, v := range klinesOut.GetKlines() {
		res = append(res, &entity.Kline{
			OpenTime:                 v.OpenTime,
			Open:                     v.Open,
			High:                     v.High,
			Low:                      v.Low,
			Close:                    v.Close,
			Volume:                   v.Volume,
			CloseTime:                v.CloseTime,
			QuoteAssetVolume:         v.QuoteAssetVolume,
			TradeNum:                 v.TradeNum,
			TakerBuyBaseAssetVolume:  v.TakerBuyBaseAssetVolume,
			TakerBuyQuoteAssetVolume: v.TakerBuyQuoteAssetVolume,
		})
	}

	return res, nil
}
func (b *BinanceClient) KlinesFrom(ctx context.Context, symbol string, period string, startTime int, limit int, market int, creds rpc.Credentials) ([]*entity.Kline, error) {
	b.log()
	var res []*entity.Kline

	KlinesOut, err := b.rpc.KlinesFrom(ctx, &rpc.KlinesFromIn{
		Credentials: &creds,
		Symbol:      symbol,
		Period:      period,
		StartTime:   int64(startTime),
		Limit:       int64(limit),
		Market:      int64(market),
	})
	if err != nil {
		return res, err
	}

	for _, v := range KlinesOut.GetKlines() {
		res = append(res, &entity.Kline{
			OpenTime:                 v.OpenTime,
			Open:                     v.Open,
			High:                     v.High,
			Low:                      v.Low,
			Close:                    v.Close,
			Volume:                   v.Volume,
			CloseTime:                v.CloseTime,
			QuoteAssetVolume:         v.QuoteAssetVolume,
			TradeNum:                 v.TradeNum,
			TakerBuyBaseAssetVolume:  v.TakerBuyBaseAssetVolume,
			TakerBuyQuoteAssetVolume: v.TakerBuyQuoteAssetVolume,
		})
	}

	return res, nil
}
func (b *BinanceClient) GetMarketStat(ctx context.Context, market int, creds rpc.Credentials) ([]*entity.PriceChangeStats, error) {
	b.log()
	var res []*entity.PriceChangeStats

	mStats, err := b.rpc.GetMarketStat(ctx, &rpc.MarketIn{
		Credentials: &creds,
		Market:      int64(market),
	})
	if err != nil {
		return res, err
	}

	for _, v := range mStats.GetPriceChangeStats() {
		res = append(res, &entity.PriceChangeStats{
			Symbol:             v.Symbol,
			PriceChange:        v.PriceChange,
			PriceChangePercent: v.PriceChangePercent,
			WeightedAvgPrice:   v.WeightedAvgPrice,
			PrevClosePrice:     v.PrevClosePrice,
			LastPrice:          v.LastPrice,
			LastQty:            v.LastQty,
			BidPrice:           v.BidPrice,
			BidQty:             v.BidQty,
			AskPrice:           v.AskPrice,
			AskQty:             v.AskQty,
			OpenPrice:          v.OpenPrice,
			HighPrice:          v.HighPrice,
			LowPrice:           v.LowPrice,
			Volume:             v.Volume,
			QuoteVolume:        v.QuoteVolume,
			OpenTime:           v.OpenTime,
			CloseTime:          v.CloseTime,
			FirstID:            v.FirstID,
			LastID:             v.LastID,
			Count:              v.Count,
		})
	}

	return res, nil
}

func (b *BinanceClient) GetOrder(ctx context.Context, symbol string, market int, creds rpc.Credentials) ([]*entity.OrderInfo, error) {
	b.log()
	var orders []*entity.OrderInfo

	oInfos, err := b.rpc.GetOrder(ctx, &rpc.OrderIn{
		Credentials: &creds,
		Symbol:      symbol,
		Market:      int64(market),
	})

	if err != nil {
		return orders, err
	}

	for _, v := range oInfos.GetOrder() {
		orders = append(orders, &entity.OrderInfo{
			Symbol:           v.Symbol,
			OrderID:          v.OrderID,
			ClientOrderID:    v.ClientOrderID,
			Price:            v.Price,
			ReduceOnly:       v.ReduceOnly,
			OrigQuantity:     v.OrigQuantity,
			ExecutedQuantity: v.ExecutedQuantity,
			CumQuantity:      v.CumQuantity,
			CumQuote:         v.CumQuote,
			Status:           v.Status,
			TimeInForce:      v.TimeInForce,
			Type:             v.Type,
			Side:             v.Side,
			StopPrice:        v.StopPrice,
			Time:             v.Time,
			UpdateTime:       v.UpdateTime,
			WorkingType:      v.WorkingType,
			ActivatePrice:    v.ActivatePrice,
			PriceRate:        v.PriceRate,
			AvgPrice:         v.AvgPrice,
			OrigType:         v.OrigType,
			PositionSide:     v.PositionSide,
			PriceProtect:     v.PriceProtect,
			ClosePosition:    v.ClosePosition,
			Market:           int(v.Market),
		})
	}

	return orders, nil
}

// Ticker define ticker info
func (b *BinanceClient) Ticker(ctx context.Context, market int, creds rpc.Credentials) ([]*entity.Crypto, error) {
	b.log()
	ticker, err := b.rpc.Ticker(ctx, &rpc.MarketIn{
		Credentials: &creds,
		Market:      int64(market),
	})
	if err != nil {
		return nil, err
	}

	var res []*entity.Crypto

	for _, v := range ticker.GetCryptos() {
		res = append(res, &entity.Crypto{
			Symbol:     v.Symbol,
			Price:      v.Price,
			LastUpdate: time.Now(),
		})
	}

	return res, nil
}

func (b *BinanceClient) CheckAvailability(timeout time.Duration) bool {
	conn, err := net.DialTimeout("tcp", b.Address, timeout)
	if err != nil {
		return false
	}
	conn.Close()
	return true
}
