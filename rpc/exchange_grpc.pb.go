// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.6.1
// source: pb/exchange.proto

package rpc

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// BinanceClient is the client API for Binance service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type BinanceClient interface {
	GetSymbols(ctx context.Context, in *MarketIn, opts ...grpc.CallOption) (*SymbolsOut, error)
	Klines(ctx context.Context, in *KlinesIn, opts ...grpc.CallOption) (*KlinesOut, error)
	KlinesFrom(ctx context.Context, in *KlinesFromIn, opts ...grpc.CallOption) (*KlinesOut, error)
	Ticker(ctx context.Context, in *MarketIn, opts ...grpc.CallOption) (*TickerOut, error)
	GetMarketStat(ctx context.Context, in *MarketIn, opts ...grpc.CallOption) (*PriceChangeStatOut, error)
	GetOrder(ctx context.Context, in *OrderIn, opts ...grpc.CallOption) (*OrderOut, error)
}

type binanceClient struct {
	cc grpc.ClientConnInterface
}

func NewBinanceClient(cc grpc.ClientConnInterface) BinanceClient {
	return &binanceClient{cc}
}

func (c *binanceClient) GetSymbols(ctx context.Context, in *MarketIn, opts ...grpc.CallOption) (*SymbolsOut, error) {
	out := new(SymbolsOut)
	err := c.cc.Invoke(ctx, "/rpc.Binance/GetSymbols", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *binanceClient) Klines(ctx context.Context, in *KlinesIn, opts ...grpc.CallOption) (*KlinesOut, error) {
	out := new(KlinesOut)
	err := c.cc.Invoke(ctx, "/rpc.Binance/Klines", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *binanceClient) KlinesFrom(ctx context.Context, in *KlinesFromIn, opts ...grpc.CallOption) (*KlinesOut, error) {
	out := new(KlinesOut)
	err := c.cc.Invoke(ctx, "/rpc.Binance/KlinesFrom", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *binanceClient) Ticker(ctx context.Context, in *MarketIn, opts ...grpc.CallOption) (*TickerOut, error) {
	out := new(TickerOut)
	err := c.cc.Invoke(ctx, "/rpc.Binance/Ticker", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *binanceClient) GetMarketStat(ctx context.Context, in *MarketIn, opts ...grpc.CallOption) (*PriceChangeStatOut, error) {
	out := new(PriceChangeStatOut)
	err := c.cc.Invoke(ctx, "/rpc.Binance/GetMarketStat", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *binanceClient) GetOrder(ctx context.Context, in *OrderIn, opts ...grpc.CallOption) (*OrderOut, error) {
	out := new(OrderOut)
	err := c.cc.Invoke(ctx, "/rpc.Binance/GetOrder", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// BinanceServer is the server API for Binance service.
// All implementations must embed UnimplementedBinanceServer
// for forward compatibility
type BinanceServer interface {
	GetSymbols(context.Context, *MarketIn) (*SymbolsOut, error)
	Klines(context.Context, *KlinesIn) (*KlinesOut, error)
	KlinesFrom(context.Context, *KlinesFromIn) (*KlinesOut, error)
	Ticker(context.Context, *MarketIn) (*TickerOut, error)
	GetMarketStat(context.Context, *MarketIn) (*PriceChangeStatOut, error)
	GetOrder(context.Context, *OrderIn) (*OrderOut, error)
	mustEmbedUnimplementedBinanceServer()
}

// UnimplementedBinanceServer must be embedded to have forward compatible implementations.
type UnimplementedBinanceServer struct {
}

func (UnimplementedBinanceServer) GetSymbols(context.Context, *MarketIn) (*SymbolsOut, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetSymbols not implemented")
}
func (UnimplementedBinanceServer) Klines(context.Context, *KlinesIn) (*KlinesOut, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Klines not implemented")
}
func (UnimplementedBinanceServer) KlinesFrom(context.Context, *KlinesFromIn) (*KlinesOut, error) {
	return nil, status.Errorf(codes.Unimplemented, "method KlinesFrom not implemented")
}
func (UnimplementedBinanceServer) Ticker(context.Context, *MarketIn) (*TickerOut, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Ticker not implemented")
}
func (UnimplementedBinanceServer) GetMarketStat(context.Context, *MarketIn) (*PriceChangeStatOut, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetMarketStat not implemented")
}
func (UnimplementedBinanceServer) GetOrder(context.Context, *OrderIn) (*OrderOut, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetOrder not implemented")
}
func (UnimplementedBinanceServer) mustEmbedUnimplementedBinanceServer() {}

// UnsafeBinanceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to BinanceServer will
// result in compilation errors.
type UnsafeBinanceServer interface {
	mustEmbedUnimplementedBinanceServer()
}

func RegisterBinanceServer(s grpc.ServiceRegistrar, srv BinanceServer) {
	s.RegisterService(&Binance_ServiceDesc, srv)
}

func _Binance_GetSymbols_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(MarketIn)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(BinanceServer).GetSymbols(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/rpc.Binance/GetSymbols",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(BinanceServer).GetSymbols(ctx, req.(*MarketIn))
	}
	return interceptor(ctx, in, info, handler)
}

func _Binance_Klines_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(KlinesIn)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(BinanceServer).Klines(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/rpc.Binance/Klines",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(BinanceServer).Klines(ctx, req.(*KlinesIn))
	}
	return interceptor(ctx, in, info, handler)
}

func _Binance_KlinesFrom_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(KlinesFromIn)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(BinanceServer).KlinesFrom(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/rpc.Binance/KlinesFrom",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(BinanceServer).KlinesFrom(ctx, req.(*KlinesFromIn))
	}
	return interceptor(ctx, in, info, handler)
}

func _Binance_Ticker_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(MarketIn)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(BinanceServer).Ticker(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/rpc.Binance/Ticker",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(BinanceServer).Ticker(ctx, req.(*MarketIn))
	}
	return interceptor(ctx, in, info, handler)
}

func _Binance_GetMarketStat_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(MarketIn)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(BinanceServer).GetMarketStat(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/rpc.Binance/GetMarketStat",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(BinanceServer).GetMarketStat(ctx, req.(*MarketIn))
	}
	return interceptor(ctx, in, info, handler)
}

func _Binance_GetOrder_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(OrderIn)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(BinanceServer).GetOrder(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/rpc.Binance/GetOrder",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(BinanceServer).GetOrder(ctx, req.(*OrderIn))
	}
	return interceptor(ctx, in, info, handler)
}

// Binance_ServiceDesc is the grpc.ServiceDesc for Binance service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var Binance_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "rpc.Binance",
	HandlerType: (*BinanceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetSymbols",
			Handler:    _Binance_GetSymbols_Handler,
		},
		{
			MethodName: "Klines",
			Handler:    _Binance_Klines_Handler,
		},
		{
			MethodName: "KlinesFrom",
			Handler:    _Binance_KlinesFrom_Handler,
		},
		{
			MethodName: "Ticker",
			Handler:    _Binance_Ticker_Handler,
		},
		{
			MethodName: "GetMarketStat",
			Handler:    _Binance_GetMarketStat_Handler,
		},
		{
			MethodName: "GetOrder",
			Handler:    _Binance_GetOrder_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "pb/exchange.proto",
}
