package server

import (
	"context"
	"gitlab.com/beearn/exchange-client"
	"gitlab.com/beearn/exchange-client-rpc/rpc"
	"go.uber.org/ratelimit"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const (
	ratePerSecond = 6
)

type BinanceServer struct {
	rpc.UnimplementedBinanceServer
	rl ratelimit.Limiter
}

func NewBinanceServer() *BinanceServer {
	return &BinanceServer{
		rl: ratelimit.New(ratePerSecond),
	}
}

func (b *BinanceServer) GetSymbols(ctx context.Context, in *rpc.MarketIn) (*rpc.SymbolsOut, error) {
	b.rl.Take()
	client := exchange.NewBinanceRateLimiter(exchange.NewBinance(in.Credentials.APIKey, in.Credentials.SecretKey))
	symbols, err := client.GetSymbols(ctx, int(in.Market))

	return &rpc.SymbolsOut{
		Symbols: symbols,
	}, err
}

func (b *BinanceServer) Klines(ctx context.Context, in *rpc.KlinesIn) (*rpc.KlinesOut, error) {
	b.rl.Take()
	client := exchange.NewBinanceRateLimiter(exchange.NewBinance(in.Credentials.APIKey, in.Credentials.SecretKey))
	klines, err := client.Klines(ctx, in.Symbol, in.Period, int(in.Market))
	if err != nil {
		return nil, err
	}

	var res rpc.KlinesOut

	for i := range klines {
		res.Klines = append(res.Klines, &rpc.Kline{
			OpenTime:                 klines[i].OpenTime,
			Open:                     klines[i].Open,
			High:                     klines[i].High,
			Low:                      klines[i].Low,
			Close:                    klines[i].Close,
			Volume:                   klines[i].Volume,
			CloseTime:                klines[i].CloseTime,
			QuoteAssetVolume:         klines[i].QuoteAssetVolume,
			TradeNum:                 klines[i].TradeNum,
			TakerBuyBaseAssetVolume:  klines[i].TakerBuyBaseAssetVolume,
			TakerBuyQuoteAssetVolume: klines[i].TakerBuyQuoteAssetVolume,
		})
	}

	return &res, nil
}
func (b *BinanceServer) KlinesFrom(ctx context.Context, in *rpc.KlinesFromIn) (*rpc.KlinesOut, error) {
	b.rl.Take()
	client := exchange.NewBinanceRateLimiter(exchange.NewBinance(in.Credentials.APIKey, in.Credentials.SecretKey))
	klines, err := client.KlinesFrom(ctx, in.Symbol, in.Period, int(in.StartTime), int(in.Limit), int(in.Market))
	if err != nil {
		return nil, err
	}

	var res rpc.KlinesOut

	for i := range klines {
		res.Klines = append(res.Klines, &rpc.Kline{
			OpenTime:                 klines[i].OpenTime,
			Open:                     klines[i].Open,
			High:                     klines[i].High,
			Low:                      klines[i].Low,
			Close:                    klines[i].Close,
			Volume:                   klines[i].Volume,
			CloseTime:                klines[i].CloseTime,
			QuoteAssetVolume:         klines[i].QuoteAssetVolume,
			TradeNum:                 klines[i].TradeNum,
			TakerBuyBaseAssetVolume:  klines[i].TakerBuyBaseAssetVolume,
			TakerBuyQuoteAssetVolume: klines[i].TakerBuyQuoteAssetVolume,
		})
	}

	return &res, nil
}

func (b *BinanceServer) Ticker(ctx context.Context, in *rpc.MarketIn) (*rpc.TickerOut, error) {
	b.rl.Take()
	client := exchange.NewBinanceRateLimiter(exchange.NewBinance(in.Credentials.APIKey, in.Credentials.SecretKey))
	cryptos, err := client.Ticker(ctx, int(in.Market))
	if err != nil {
		return nil, err
	}

	var res rpc.TickerOut

	for i := range cryptos {
		res.Cryptos = append(res.Cryptos, &rpc.Crypto{
			Symbol: cryptos[i].Symbol,
			Price:  cryptos[i].Price,
		})
	}

	return &res, nil
}

func (b *BinanceServer) GetMarketStat(ctx context.Context, in *rpc.MarketIn) (*rpc.PriceChangeStatOut, error) {
	b.rl.Take()
	client := exchange.NewBinanceRateLimiter(exchange.NewBinance(in.Credentials.APIKey, in.Credentials.SecretKey))
	priceChangeStats, err := client.GetMarketStat(ctx, int(in.Market))
	if err != nil {
		return nil, err
	}

	var res rpc.PriceChangeStatOut

	for i := range priceChangeStats {
		res.PriceChangeStats = append(res.PriceChangeStats, &rpc.PriceChangeStat{
			Symbol:             priceChangeStats[i].Symbol,
			PriceChange:        priceChangeStats[i].PriceChange,
			PriceChangePercent: priceChangeStats[i].PriceChangePercent,
			WeightedAvgPrice:   priceChangeStats[i].WeightedAvgPrice,
			PrevClosePrice:     priceChangeStats[i].PrevClosePrice,
			LastPrice:          priceChangeStats[i].LastPrice,
			LastQty:            priceChangeStats[i].LastQty,
			BidPrice:           priceChangeStats[i].BidPrice,
			BidQty:             priceChangeStats[i].BidQty,
			AskPrice:           priceChangeStats[i].AskPrice,
			AskQty:             priceChangeStats[i].AskQty,
			OpenPrice:          priceChangeStats[i].OpenPrice,
			HighPrice:          priceChangeStats[i].HighPrice,
			LowPrice:           priceChangeStats[i].LowPrice,
			Volume:             priceChangeStats[i].Volume,
			QuoteVolume:        priceChangeStats[i].QuoteVolume,
			OpenTime:           priceChangeStats[i].OpenTime,
			CloseTime:          priceChangeStats[i].CloseTime,
			FirstID:            priceChangeStats[i].FirstID,
			LastID:             priceChangeStats[i].LastID,
			Count:              priceChangeStats[i].Count,
		})
	}

	return &res, nil
}

func (b *BinanceServer) GetOrder(context.Context, *rpc.OrderIn) (*rpc.OrderOut, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetOrder not implemented")
}
